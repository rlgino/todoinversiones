package todoinversiones

class BootStrap {

    def init = { servletContext ->
        def admin = Usuario.findByMail("asd@asd.com")
        if(admin == null){
            createAdminUser()
            createCategorias()
            createPreguntaTest()
        }else
            println("Se encontro admin")
    }

    def createPreguntaTest(){
        def preg = new PreguntaTestInversor(
            id:1,
            pregunta : "¿ A cuanto tiempo desea invertir el dinero ?", 
            opcionA : " 6 Meses.",
            opcionB : " 1 Año.",
            opcionC : " 3 Años.",
            puntajeOpcionA : 5,
            puntajeOpcionB : 10,
            puntajeOpcionC : 20 
            )
        .save(flush:true)

        def preg2 = new PreguntaTestInversor(
            id:2,
            pregunta : "Un activo en el que se invierte baja un 10%: ¿ Que hace ?", 
            opcionA : " Retirar el dinero",
            opcionB : " Esperar (no hacer nada)",
            opcionC : " Invertir mas dinero",
            puntajeOpcionA : 5,
            puntajeOpcionB : 10,
            puntajeOpcionC : 20 
            )
        .save(flush:true)

        def preg3 = new PreguntaTestInversor(
            id:3,
            pregunta : "Un activo en el que se invierte baja un 25%: ¿ Que hace ?", 
            opcionA : " Retirar el dinero",
            opcionB : " Esperar (no hacer nada)",
            opcionC : " Invertir mas dinero",
            puntajeOpcionA : 5,
            puntajeOpcionB : 10,
            puntajeOpcionC : 20 
            )
        .save(flush:true)

        def preg4 = new PreguntaTestInversor(
            id:4,
            pregunta : "¿ Cual es su experiencia de inversion ?", 
            opcionA : "Poca experiencia (menoar a 1 año).",
            opcionB : "Experencia intermedia (de 1 a 2 años)",
            opcionC : "Experiencia avanzada (mas de 3 años)",
            puntajeOpcionA : 5,
            puntajeOpcionB : 10,
            puntajeOpcionC : 20 
            )
        .save(flush:true)

        def preg5 = new PreguntaTestInversor(
            id:5,
            pregunta : "¿ Cuales son sus preferencias de inversion ?", 
            opcionA : "Inversion en renta fija.",
            opcionB : "Inversion en fondos comunes de inversion.",
            opcionC : "Inversion en renta variada.",
            puntajeOpcionA : 5,
            puntajeOpcionB : 10,
            puntajeOpcionC : 20 
            )
        .save(flush:true)

    }

    def destroy = {
    }

    def createAdminUser(){
        def result = new Usuario()
        result.dni = "12341234"
        result.nombre = "Admin"
        result.apellido = "Admin"
        result.mail = "asd@asd.com"
        result.fechaNac = new Date()
        result.cont = "123123123"
        result.esCeo = true
        result.esAdmin = true
        result.save(flush: true)
        println("Admin creado con ID " + result.id)
    }

    def createCategorias(){
        def bonos = new Categoria()
        bonos.nombre = "Bono"
        bonos.save(flush: true)
        crearBonos(bonos)

        def acciones = new Categoria()
        acciones.nombre = "Acciones"
        acciones.save(flush: true)
        crearAcciones(acciones)
        
        def cripto = new Categoria()
        cripto.nombre = "Criptomoneda"
        cripto.save(flush: true)
        crearCriptos(cripto)
        
        def fondo = new Categoria()
        fondo.nombre = "Fondo"
        fondo.save(flush: true)
    }

    def crearBonos(Categoria bonos){
        //Dolar
        def prod = new Producto(
            nombre: "Dolar",
            cotizacionVenta: 45,
            cotizacionCompra: 43,
            descripcion: "Valor moneda extranjera",
            categoria: bonos
        )
        prod.save(flush: true, validate: false)
        println("Producto creado con id " + prod.id)
        //Peso argentino
        def prod1 = new Producto(
            nombre: "Peso",
            cotizacionVenta: 1,
            cotizacionCompra: 1,
            descripcion: "Valor moneda local",
            categoria: bonos
        )
        prod1.save(flush: true, validate: false)
        println("Producto creado con id " + prod1.id)
    }

    def crearCriptos(Categoria cripto){
        //BTC
        def prod = new Producto(
            nombre: "BTC",
            cotizacionVenta: 3,
            cotizacionCompra: 2,
            descripcion: "Valor moneda digital",
            categoria: cripto
        )
        prod.save(flush: true, validate: false)
        println("Producto creado con id " + prod.id)
        //ETH
        def prod1 = new Producto(
            nombre: "ETH",
            cotizacionVenta: 3,
            cotizacionCompra: 2,
            descripcion: "Valor moneda digital",
            categoria: cripto
        )
        prod1.save(flush: true, validate: false)
        println("Producto creado con id " + prod1.id)
        //BNB
        def prod2 = new Producto(
            nombre: "BNB",
            cotizacionVenta: 3,
            cotizacionCompra: 2,
            descripcion: "Valor moneda digital",
            categoria: cripto
        )
        prod2.save(flush: true, validate: false)
        println("Producto creado con id " + prod2.id)
    }

    def crearAcciones(Categoria acciones){
    }
}
