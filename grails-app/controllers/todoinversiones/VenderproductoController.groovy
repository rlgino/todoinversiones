package todoinversiones

import seguridad.UsuarioService

class VenderproductoController {

    def index() { 
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        def cliente = Cliente.findByUsuario(session.usuario)
        if(cliente == null){
            flash.message = "Error con usuario, no es cliente"
            return
        }

        def idInv = params.idInversion
        if(idInv == null) idInv = 0

        def inversiones = Inversion.findAllByCliente(cliente)
        def acum = 0
        for(inv in inversiones){
            if(inv.fechaFin == null)
                acum = acum + inv.monto * inv.producto.cotizacionCompra
        }

        ["inversiones": inversiones, "inversion": Inversion.findById(idInv), "total": acum]
    }

    def venderProducto(){
        def id = params.idInv
        println("Se va a finalizar la inversion " + id)
        def inversion = Inversion.findById(id)
        inversion.fechaFin = new Date()
        inversion.save(flush: true)
        
        flash.message = actualizarCuenta(Double.parseDouble(params.total))

        redirect(action: "index")
    }

    def actualizarCuenta(Double total){
        def cliente = Cliente.findByUsuario(session.usuario)
        if(cliente == null)
            return "Error con usuario, no es cliente"
        def cuenta = CuentaComitente.findByCliente(cliente)
        
        cuenta.monto = cuenta.monto + total
        cuenta.save(flush:true)
        return null
    }
}
