package todoinversiones

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/login/index")
        "/signup"(view:"/login/signup")
        "/categoria"(view:"/categoria")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
