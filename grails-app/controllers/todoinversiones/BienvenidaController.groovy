package todoinversiones

import seguridad.UsuarioService

class BienvenidaController {

    def index() {
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }
        ['usuario': UsuarioService.usuario]
        def cliente = Cliente.findByUsuario(UsuarioService.usuario)
        if(cliente != null && !cliente.hizoEncuesta)
            redirect(controller: "testinversion")
    }

}
