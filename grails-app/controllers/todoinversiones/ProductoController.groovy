package todoinversiones

import seguridad.UsuarioService

class ProductoController {

    
    def index() {



        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        if( !session.esAdmin ){
            redirect(action: '404')
            return
        }
        
        if (params.id != null){
            println("\nId seleccionado " + params.id)
            flash.message = Producto.findById(params.id).nombre
        }
        ['categorias':  Categoria.findAll(),'productos':  Producto.findAll()]
    }

    def producto() {
        println("Categoria elegida: " + params.categoria)
        def prod = new Producto()
        prod.nombre = params.nombre
        prod.cotizacionVenta=new BigDecimal(params.cotizacionVenta);
        prod.cotizacionCompra=new BigDecimal(params.cotizacionCompra);
        prod.descripcion=params.descripcion
        def cat = Categoria.findById(params.categoria)
        if(cat != null)
            prod.categoria=cat
        else{
            flash.message = "Error en categoria"
            return;
        }
        prod.save(flush: true)
        redirect(action: 'index')
    }

    def comprar(){ }

    def vender(){ }

}

