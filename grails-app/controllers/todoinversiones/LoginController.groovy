package todoinversiones

import seguridad.UsuarioService

class LoginController {

    def index() { }

    def login() {
        if(params.documento == null || params.password == null ) return
        Usuario result = Usuario.findByDni(params.documento)
        if( result != null && params.password == result.cont) {
            session.user = result.nombre
            session.usuario = result
            session.esAdmin = result.esAdmin
            UsuarioService.usuario = result
            redirect(controller: 'bienvenida')
        }
        else{
            flash.message = "Error en credenciales"
            redirect(action: 'index')
        }
    }

    def signup() {
        def error = validarUsuario()
        if(!error.isEmpty()){
            flash.message = error
            return;
        }
        def result = new Usuario()
        result.dni = params.dni
        result.nombre = params.nombre
        result.apellido = params.apellido
        result.mail = params.mail
        result.fechaNac = Date.parse("yyyy-MM-dd", params.fechanac)
        result.cont = params.password
        result.esCeo = false
        result.esAdmin = false
        result.save(flush: true)
        println("Usuario creado con id " + result.id)
        def cliente = new Cliente()
        cliente.usuario = result
        cliente.banco = params.banco
        cliente.hizoEncuesta = false
        cliente.perfilInversor = ""
        cliente.save(flush: true)
        println("Clienet creado con id " + cliente.id)
        def cuenta = new CuentaComitente()
        cuenta.monto = new BigDecimal(0);
        cuenta.cliente = cliente
        cuenta.fecApertura = new Date()
        cuenta.save(flush:true)
        println("Cuenta comitente creada con id " + cuenta.id)
        //TODO guardar cliente
        redirect(action: 'index')
    }

    def validarUsuario(){
        if(params.mail == null || params.password == null ) {
            return "Faltan campos"
            
        }
        if(params.mail == null || params.password == null ) {
            return "Faltan campos"
        }
        if(params.password.length() < 8) {
            return "Password incorrecta"
        }
        if(params.mail != null && Usuario.findByMail(params.mail) != null && Usuario.findByDni(params.dni) != null){
            return "Usuario existente"
        }
        return ""
    }

    def logout() {
        UsuarioService.usuario = null
        session.user = null
        redirect(action: 'index')
    }
}
