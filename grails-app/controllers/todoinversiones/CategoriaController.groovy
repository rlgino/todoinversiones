package todoinversiones

import seguridad.UsuarioService

class CategoriaController {

    def index() {
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        if( !session.esAdmin ){
            redirect(action: '404')
            return
        }
        
        flash.message = ""
        if (params.id != null){
            println("\nId seleccionado " + params.id)
            flash.message = Categoria.findById(params.id).nombre
        }
        ['categorias':  Categoria.findAll(), 'id': params.id, 'nombre': flash.message]
    }

    def categoria() {
        def cat = new Categoria()
        cat.nombre = params.nombre
        cat.save(flush: true)
        redirect(action: 'index')
    }
}
