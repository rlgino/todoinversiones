package todoinversiones

class PerfilController {

    def index() {
        def cliente = Cliente.findByUsuario(session.usuario)

        if(params.saldo != null){
            println("Nuevo saldo: " + params.saldo)
            def cuenta = CuentaComitente.findByCliente(cliente)
            cuenta.monto = cuenta.monto + Double.parseDouble(params.saldo)
            cuenta.save(flush:true)
        }

        ["saldo": CuentaComitente.findByCliente(cliente).monto , "banco": cliente.banco]
    }

    def cargar(){
        println("Nuevo saldo: " + params.saldo)
        println("Nuevo saldo: " + params.saldo)
        println("Nuevo saldo: " + params.saldo)
        println("Nuevo saldo: " + params.saldo)
        println("Nuevo saldo: " + params.saldo)
        println("Nuevo saldo: " + params.saldo)

        redirect(action: 'index')
    }
}
