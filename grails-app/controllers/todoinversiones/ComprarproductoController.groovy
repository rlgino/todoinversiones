package todoinversiones

import seguridad.UsuarioService

class ComprarproductoController {

    def index() { 
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }
        
        ["categorias": Categoria.findAll()]
    }

    def catSeleccionada(){
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        def id = 0
        if(params.idCat != null)
            id = params.idCat
        
        println(id)
        
        ["categorias": Categoria.findAll(), "idCat": Categoria.findById(id).id, "productos": Producto.findAllByCategoria(Categoria.findById(id))]
    }

    def invertir(){
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        def id = 0
        if(params.idCat != null)
            id = params.idCat
        
        def idProd = 0
        if(params.idProd != null)
            idProd = params.idProd

        ["categorias": Categoria.findAll(), 
            "idCat": Categoria.findById(id).id, 
            "productos": Producto.findAllByCategoria(Categoria.findById(id)),
            "producto": Producto.findById(idProd),
            "idProd": Producto.findById(idProd).id,
            "montoProd": Producto.findById(idProd).cotizacionVenta]
    }

    def finalizarInversion(){
        println("Costo de la inversion " + params.total)
        def prodId = params.prodId
        println("Producto elegido " + prodId)
        if(params.total != null){
            def total = Double.parseDouble(params.total)
            def nuevaInversion = new Inversion()
            def prod = Producto.findById(prodId)
            nuevaInversion.producto = prod
            nuevaInversion.monto = Double.parseDouble(params.total)/prod.cotizacionVenta
            nuevaInversion.fechaInversion = new Date()
            def result = validarMonto(nuevaInversion, total)

            
            if(result != null){
                flash.message = result
            }
        }
        redirect(action: 'index')
    }

    def validarMonto(nuevaInversion, Double total){
        def cliente = Cliente.findByUsuario(session.usuario)
        if(cliente == null)
            return "Error con usuario, no es cliente"
        def cuenta = CuentaComitente.findByCliente(cliente)
        if(cuenta.monto < total)
            return "Error con monto, es insuficiente"
        nuevaInversion.cliente = cliente
        hacerInversion(nuevaInversion)
        
        cuenta.monto = cuenta.monto - total
        println("Nuevo monto " + cuenta.monto)
        cuenta.save(flush: true)
        return null
    }

    def hacerInversion(Inversion nuevaInversion){
        nuevaInversion.save(flush: true, validate: false)
        println(nuevaInversion.id)
    }
}
