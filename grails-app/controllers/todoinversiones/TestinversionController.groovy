package todoinversiones

import seguridad.UsuarioService

class TestinversionController {

    def index() {
        if(UsuarioService.usuario == null) {
            redirect(controller: 'login', action: 'index')
            return
        }

        ['preguntas':  PreguntaTestInversor.findAll()]
    }

    def calcularPerfil(){

        if(params.op1 == null || params.op2 == null || params.op3 == null || params.op4 == null || params.op5 == null) 
            {
                flash.message = "Responda todas las preguntas."
                redirect (action : "index")
            }
        else{
                redirect (action : "mostrarResultado")
            }

        def user_result = Usuario.findByNombre(session.user)

        println("Nombre cliente: " + user_result.nombre)  

        def cliente_result = Cliente.findByUsuario(user_result)

        println("banco: " + cliente_result.banco)

        def resultado = 0

        def perfil = "ninguno"

        def cantPreguntas = params.cantPreguntas
       // println("\ncantidad preguntas: " + cantPreguntas)

       if ( !(params.op1 == null || params.op2 == null || params.op3 == null || params.op4 == null || params.op5 == null))
       {
        def op1 = params.op1
            def op2 = params.op2
            def op3 = params.op3
            def op4 = params.op4 
            def op5 = params.op5

            //if(op1 != null ) println("El valor de la op1 elegida es " + op1)
            def val_op1 = op1.toInteger()
            def val_op2 = op2.toInteger()
            def val_op3 = op3.toInteger()
            def val_op4 = op4.toInteger()
            def val_op5 = op5.toInteger()

            resultado += val_op1 + val_op2 + val_op3 + val_op4 + val_op5
       }

        println("\nResultado : " + resultado)
        
        if(resultado == 0 )
            perfil = "Sin definir"
        else if ( resultado <30 )
            perfil = "Arriesgado"
        else if(resultado < 60)
            perfil = "Moderador"
        else
            perfil = "Conservador"

        println("El perfil es: " + perfil ) 

        cliente_result.perfilInversor = perfil
        cliente_result.save(flush:true)

        println("Perfil del cliente " + user_result.nombre + ": " + cliente_result.perfilInversor)

        

    }

    def mostrarResultado(){

        def user_result = Usuario.findByNombre(session.user)

        def cliente_result = Cliente.findByUsuario(user_result)      
        cliente_result.hizoEncuesta=true
        cliente_result.save(flush:true)  

        ['usuario' : user_result,

         'cliente' : cliente_result ]
    }

}