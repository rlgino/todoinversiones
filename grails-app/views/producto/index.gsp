<!doctype html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Productos</title>

</head>


<body>

<main role="main" class="flex-shrink-0">
<g:form class="form-horizontal" style="margin:0 auto" action="producto" controller="producto">
 <div class="container">
        <section class="row">
            <div class="col-md-12">
                <h3 class="text-left">Administrar productos</h3>
            </div>
        </section>
            <g:if test="${ flash.message }">
                <div class="alert alert-danger" role="alert">
                    ${flash.message}
                </div>
            </g:if>

        <div class="form-row col-md-6 center_div">
            <div class="form-group col-md-12">
                <label for="nombre" class="control-label">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
            </div>
                        
            <div class="form-group col-md-12"> 
                <label for="cotizacionVenta" class="control-label">Cotización venta</label>
                <input type="number" class="form-control" id="cotizacionVenta" name="cotizacionVenta" min="0" placeholder="Cotización Venta" step=".01" required>
            </div>    

            <div class="form-group col-md-12"> 
                <label for="cotizacionCompra" class="control-label" >Cotización compra</label>
                <input type="number" class="form-control" id="cotizacionCompra" name="cotizacionCompra" min="0" placeholder="Cotización compra" step=".01" required>
            </div>                                    
                                    
            <div class="form-group col-md-12"> 
                <label for="categoria" class="control-label">Categoria</label>
                <select class="form-control" id="categoria" name="categoria">
                    <g:each var="cat" in="${ categorias }">
                        <option value="${cat.id}">${ cat.nombre }</option>
                    </g:each>
                </select>                    
            </div>
            

            <div class="form-group col-md-12"> 
                <label for="descripcion" class="control-label">Descripción</label>
                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" required>
            </div>        
            
            <div class="form-group col-md-12"> 
            <button class="btn btn-lg btn-primary btn-block" type="submit">Agregar</button>
            </div>  
        </div>  
   

 </g:form> 
 
 <ul class="list-group col-md-4" style="margin-left:100px">
        <g:each in="${productos}" var="prod">
            <g:if test="${flash.message == prod.nombre}">
                <li class="list-group-item active">${prod.id} - ${prod.nombre}</li>
            </g:if>
            <g:else>
                <li class="list-group-item"><a href="/producto/index?id=${prod.id}">${prod.id} - ${prod.nombre} - ${prod.cotizacionVenta}  - ${prod.cotizacionCompra} - ${prod.descripcion} - ${prod.categoria}
                 </a></li>
            </g:else>
        </g:each>
        <li class="list-group-item"><a href="/producto/index">Agregar producto...</a></li>
    </ul>
     </div>
</main>
</body>
</html>