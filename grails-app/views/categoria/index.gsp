<!doctype html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Categorias</title>

</head>
<body class="d-flex flex-column h-100">

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
    <div class="container">
        <h1>
            <g:if test="${flash.message}">
                ${flash.message}
            </g:if>
            <g:else>
                Categoria
            </g:else>
        </h1>
        <g:form action="categoria" controller="categoria">
            <div class="form-group col-md-4"> 
                <label for="nombre" class="control-label">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre" required>
            </div> 
        
            <div class="form-group col-md-4"> 
                <button class="btn btn-lg btn-primary btn-block" type="submit">Agregar</button>
            </div>  
        </g:form>
        
    <ul class="list-group col-md-4" style="margin-left:100px">
        <g:each in="${categorias}" var="cat">
            <g:if test="${flash.message == cat.nombre}">
                <li class="list-group-item active">${cat.id} - ${cat.nombre}</li>
            </g:if>
            <g:else>
                <li class="list-group-item"><a href="/categoria/index?id=${cat.id}">${cat.id} - ${cat.nombre}</a></li>
            </g:else>
        </g:each>
        <li class="list-group-item"><a href="/categoria/index">Agregar categoria...</a></li>
    </ul>
    </div>

</main>
</body>
</html>
