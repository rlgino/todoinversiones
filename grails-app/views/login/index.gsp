<!doctype html>
<html xmlns:g="http://www.w3.org/1999/XSL/Transform">
  <head>
    <meta name="layout" content="main" />
    <title>Login</title>

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.3/examples/floating-labels/floating-labels.css" rel="stylesheet">
</head>
<body>
      <g:form class="form-signin" action="login" controller="login">
        <div class="text-center mb-4">
          <img class="mb-4" src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <g:if test="${flash.message}">
                <div class="alert alert-danger" role="alert">
                    ${flash.message}
                </div>
            </g:if>
        </div>

        <div class="form-label-group">
          <input type="number" documento="mail" name="documento" class="form-control" placeholder="documento" required autofocus>
          <label for="documento">Documento</label>
        </div>

        <div class="form-label-group">
          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
          <label for="inputPassword">Password</label>
        </div>

        <div class="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
          <a class="nav-link" href="/signup">Signup</a>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </g:form>
</body>
</html>
