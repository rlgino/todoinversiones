<!doctype html>
<html xmlns:g="http://www.w3.org/1999/XSL/Transform">
    <head>
        <meta name="layout" content="main" />
        <title>Sign up</title>
        <link href="https://getbootstrap.com/docs/4.3/examples/floating-labels/floating-labels.css" rel="stylesheet">
    </head>
<body>

  
  <script type = "text/JavaScript">
      



   function validarForm(){

  var expRegNombre=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
  var expRegApellidos=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
  var expRegbanco=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
  var expRegCorreo=/^\w+@(\w+\.)+\w{2,4}$/; 
  var ex_regular_dni= /^\d{8}(?:[-\s]\d{4})?$/;


     var dni = document.getElementById("dni");
     var nombre = document.getElementById("nombre");
     
     var apellidos = document.getElementById("apellido");
     var correo = document.getElementById("mail");
     
     var banco = document.getElementById("banco");
     //Campo nombre


  if(!ex_regular_dni.exec(dni.value)){
     alert('Dni erroneo, formato no válido');
     return  false;
   }
     

     if(!nombre.value)
     {
      alert("El campo nombre es requerido");
      nombre.focus();
      return false;
     }
     if (!expRegNombre.exec(nombre.value))
     {
        alert("El campo nombre admite letras y espacios.")
        nombre.focus();
        return false;
     }
     //Campo apellido
     if(!apellidos.value)
     {
      alert("El campo apellidos es requerido");
      apellidos.focus();
      return false;
     }
     if(!expRegApellidos.exec(apellidos.value))
     {
       alert("El campo apellidos admite letras y espacios.")
       apellidos.focus();
       return false;
     }

     //Campo banco
     if(!banco.value)
     {
      alert("El campo banco es requerido");
      banco.focus();
      return false;
     }
     if(!expRegbanco.exec(banco.value))
     {
       alert("El campo banco admite letras y espacios.")
       banco.focus();
       return false;
     }
     //campo email
     if(!correo.value)
     {
      alert("El campo correo es requerido");
      correo.focus();
      return false;
     }
     if(!expRegCorreo.exec(correo.value))
     {
       alert("El campo correo no tiene el formato correcto.")
       correo.focus();
       return false;
     }
   

   return true;
}


      </script>
    <g:form class="form-signin" action="signup" controller="login" onsubmit="return validarForm()">
        <div class="text-center mb-4">
          <img class="mb-4" src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <g:if test="${flash.message}">
                <div class="alert alert-danger" role="danger">
                    ${flash.message}
                </div>
            </g:if>
        </div>

        <div class="form-label-group">
            <input type="text"  id="dni" name="dni"  class="form-control"  placeholder="dni" required autofocus>
            <label for="dni">Documento</label>
        </div>

        <div class="form-label-group">
            <input type="text" id="nombre" name="nombre"  class="form-control" placeholder="nombre" required autofocus>
            <label for="nombre">Nombre</label>
        </div>

        <div class="form-label-group">
            <input type="text" id="apellido" name="apellido" class="form-control" placeholder="apellido" required autofocus>
            <label for="apellido">Apellido</label>
        </div>

        <div class="form-label-group">
            <input type="date" id="fechanac" name="fechanac" class="form-control" placeholder="dd-mm-aaaa" required autofocus>
            <label for="fechanac">Fecha de nacimiento</label>
        </div>

        <div class="form-label-group">
            <input type="text" id="banco" name="banco" class="form-control" placeholder="banco" required autofocus>
            <label for="banco">Banco</label>
        </div>

        <div class="form-label-group">
        <input type="email" id="mail" name="mail" class="form-control" placeholder="Email address" required autofocus>
        <label for="mail">Email address</label>
        </div>

        <div class="form-label-group">
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <label for="inputPassword">Password</label>
        </div>
        <a class="nav-link" href="/login">Log in</a>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    </g:form>
</body>
</html>

