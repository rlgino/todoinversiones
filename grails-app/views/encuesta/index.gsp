
<!doctype html>
<html>
        <head>
       <title>Encuesta de satisfacción</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="layout" content="main" />
  <title>Encuesta de satisfacción</title>

</head>


<body>


<g:form class="form-horizontal" style="margin:0 auto" action="index" controller="producto">
  <br />
  <div class="container">
    <section class="row">
      <div class="col-md-12">
        <h1 class="text-center">Formato de Encuesta de Satisfacción.</h1>
        <p class="text-center">TODO INVERSIONES</p>
      </div>
    </section>
    <br />
  

    <!--  PREGUNTA 1  -->
    <section class="row">
      <div class="col-md-6">
        <p>1- ¿Que tan probable es que recomiendes el portal a tus familiares y amigos?</p>
      </div>
      <div class="col-md-2">
        <label class="radio">
        <input type="number"
       min="0"
       max="10"
       step="2"
       value="0">
      </div>


    </section>
    <!--  PREGUNTA 2  -->
    <section class="row">
      <div class="col-md-6">
        <p>2- ¿Qué tan amigable le resulta el sitio?</p>
      </div>
        <div class="col-md-2">
        <label class="radio">
        <input type="number"
       min="0"
       max="10"
       step="2"
       value="0">
      </div>
    </section>
    
    <!--  PREGUNTA 4  -->
    <section class="row">
      <div class="col-md-6">
        <p>3- La información que le ofrecio el sistema, ¿le resulto útil?</p>
      </div>
      <div class="col-md-2">
        <label class="radio">
        <input type="radio" name="pregunta1" id="pregunta1a" value="SI"> POCO
      </label>
      </div>
      <div class="col-md-2">
        <label class="radio">
        <input type="radio" name="pregunta1" id="preguntab" value="NO"> MUCHO
      </label>
      </div>
      <div class="col-md-2">
        <label class="radio">
        <input type="radio" name="pregunta1" id="preguntac" value="NA">EXCELENTE
      </label>
      </div>
    </section><br>
    <hr>
    
    <!--  PREGUNTA 5  -->
    <section class="row">
      <div class="col-md-6">
        <p>4-¿Recibio soporte a tiempo?</p>
      </div>


                                   
    <div class="form-group col-md-12"> 
        <select class="form-control" id="cateogria_id">
            <option value="AL">EXTREMADAMENTE RAPIDO</option>
            <option value="AK">MUY RAPIDO</option>
            <option value="AZ">LIGERAMENTE RAPIDO </option>
            <option value="AR">POCO RAPIDO </option>
        </select>                    
    </div>
    
    </section>
    <!--  PREGUNTA 6  -->
    <section class="row">
      <div class="col-md-6">
        <p>5- ¿Sus preguntas fueron contestadas puntualmente?</p>
      </div>
                                
    <div class="form-group col-md-12"> 
        <select class="form-control" id="cateogria_id">
            <option value="AL">ESTOY TOTALMENTE DE ACUERDO</option>
            <option value="AK">ESTOY DE ACUERDO</option>
            <option value="AZ">NO ESTOY DE ACUERDO</option>
            <option value="AR">NO ESTOY EN ABSOLUTO DE ACUERDO</option>
        </select>                    
    </div>
    
    </section>
    <!--  PREGUNTA 7  -->
    <section class="row">
      <div class="col-md-6">
        <p>6- ¿Las respuestas fueron claras y concisas?</p>
      </div>


                                 
    <div class="form-group col-md-12"> 
        <select class="form-control" id="cateogria_id">
            <option value="AL">ESTOY TOTALMENTE DE ACUERDO</option>
            <option value="AK">ESTOY DE ACUERDO</option>
            <option value="AZ">NO ESTOY DE ACUERDO</option>
            <option value="AR">Criptomoneda</option>
        </select>                    
    </div>
    </section>
  

    <section class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="comment">7- ¿Qué aspectos del sitio debemos modificar para que nos recomiendes?</label>
          <textarea class="form-control" rows="6" id="comentarios"></textarea>
        </div>
      </div>
    </section>


    <!--  Comentarios  -->
    <section class="row">
      <div class="col-md-8">
        <h3>Comentarios</h3>
        <p></p>
      </div>
    </section>
    <section class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="comment">Comentarios:</label>
          <textarea class="form-control" rows="6" id="comentarios"></textarea>
        </div>
      </div>
    </section>
    <section class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-info" id="saveForm" onclick="saveForm">Guardar Encuesta</button>
        <button type="button" class="btn btn-danger" id="clearForm">Limpiar formulario</button>
      </div>
    </section>
      <div style="    padding: 40px 0;
    background-color: #f5f5f5;
    color: #4b4c4d;">
            <footer>
                <p style="margin-top: 15px;
    text-align: center;
    font-size: 13px;
    color: #aaa;
    margin-bottom: 0;">Todos los derechos reservados © 2019</p>
                <p style="margin-top: 15px;
    text-align: center;
    font-size: 13px;
    color: #aaa;
    margin-bottom: 0;">v 1.0.5</p>
            </footer>
        </div>
  </div>


  
 

</g:form>

</body>
</html>












        