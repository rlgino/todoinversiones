<!doctype html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Vender producto</title>

    <style>
     table{
    table-layout: fixed;
    width: 250px;
}

th, td {
    border: 1px solid blue;
    width: 100px;
    word-wrap: break-word;
}
    </style>
    
</head>

<body>
    <div class="container">
        <g:if test="${total <= 0}">
           <a class="btn btn-primary" href="/comprarproducto/index" role="button">Invertir nuevamente</a>
        </g:if>
		<ul class="list-group">
		<g:each var="inv" in="${ inversiones }">
            <g:if test="${ inversion != null && inversion.id == inv.id && inv.fechaFin == null}">
                <li class="list-group-item active">
                    ${ inv.producto.nombre } - ${ inv.monto }
                </li>       
            </g:if>
            <g:elseif test="${ inversion != null && inversion.id == inv.id }">
               ${ inv.producto.nombre } - ${ inv.monto } (Finalizada el ${inv.fechaFin})
            </g:elseif>
            <g:else>
               <li class="list-group-item">
                    <g:if test="${ inv.fechaFin != null }">
                        ${ inv.producto.nombre } - ${ inv.monto } <b>Finalizada el ${ inv.fechaFin } </b>
                    </g:if>
                    <g:else>
                        <a href="/venderproducto/index?idInversion=${inv.id}">
                            ${ inv.producto.nombre } - ${ inv.monto } 
                        </a>
                    </g:else>
                </li>
            </g:else>
		</g:each>
			<li class="list-group-item" style="text-align:right">TOTAL ${total}</li>
		</ul>
        <g:if test="${ inversion != null }">
            <g:form class="form-horizontal" style="margin:0 auto" action="venderProducto" controller="venderproducto">
                <div class="form-group col-md-4" style="display: inline-block"> 
                    <input type="hidden" name="idInv" value="${ inversion.id }" />
                    <label for="monto" class="control-label">Monto a vender</label>
                    <input type="number" class="form-control" id="monto" name="monto" placeholder="monto" value="${ inversion.monto }" step=".01" onchange="cambiarMonto()" readonly required>
                    Valor total Resultante (AR$):
                    <input type="number" class="form-control" id="total" name="total" value="${inversion.producto.cotizacionCompra * inversion.monto}" step=".01" required readonly>
                    <br />
                    <button class="btn btn-lg btn-primary btn-block" id="invertir" type="submit">Finalizar inversion</button>
                </div>  

                <div class="form-group col-md-4" style="display: inline-block"> 
                    Valor para la compra: AR$ <strong>${inversion.producto.cotizacionCompra} </strong><br />
                    Valor para la venta: AR$ ${inversion.producto.cotizacionVenta}
                </div> 
            </g:form>
        </g:if>
    </div>
</body>