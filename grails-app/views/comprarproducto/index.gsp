<!doctype html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hacer inversion</title>
</head>

<body>
	<div class="container">
		
		<g:if test="${flash.message}">
            <div class="alert alert-danger" role="alert">
        		${flash.message}
            </div>
    	</g:if>

		<g:form class="form-horizontal" style="margin:0 auto" action="comprar" controller="producto">
			<ul class="list-group col-md-8 list-group-horizontal" style="margin-left:100px">
			<g:each in="${categorias}" var="cat">
				<g:if test="${ idCat == cat.id}">
					<li class="list-group-item active">${cat.id} - ${cat.nombre}</li>
				</g:if>
				<g:else>
					<li class="list-group-item"><a href="/comprarproducto/catSeleccionada?idCat=${cat.id}">${cat.id} - ${cat.nombre}</a></li>
				</g:else>
			</g:each>
		</ul>
		</g:form>	


	</div>
</body>