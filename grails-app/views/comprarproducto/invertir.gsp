<!doctype html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hacer inversion</title>
</head>

<body>
	<div class="container">
		<ul class="list-group list-group-horizontal" style="margin-left:100px">
			<g:each in="${categorias}" var="cat">
				<g:if test="${ idCat == cat.id}">
					<li class="list-group-item active">${cat.id} - ${cat.nombre}</li>
				</g:if>
				<g:else>
					<li class="list-group-item"><a href="/comprarproducto/catSeleccionada?idCat=${cat.id}">${cat.id} - ${cat.nombre}</a></li>
				</g:else>
			</g:each>
		</ul>

		<br />
		
		<ul class="list-group list-group-horizontal" style="margin-left:100px">
			<g:each in="${productos}" var="prod">
				<g:if test="${ prod.id == idProd }">
					<li class="list-group-item active">
						
							${prod.id} - ${prod.nombre}
						
					</li>
				   
				</g:if>
				<g:else>
					<li class="list-group-item">
						<a href="/comprarproducto/invertir?idCat=${idCat}&idProd=${idProd}">
							${prod.id} - ${prod.nombre}
						</a>
					</li>
				</g:else>
			</g:each>
		</ul>

		<br />
		<g:form action="finalizarInversion" controller="Comprarproducto">
            <div class="form-group col-md-4" style="display: inline-block"> 
				<input type="hidden" name="prodId" value="${ idProd }" />
                <input type="hidden" class="form-control" id="montoProd" name="montoProd" value="${montoProd}" step=".01" required>
                <label for="monto" class="control-label">Monto a comprar</label>
                <input type="number" class="form-control" id="monto" name="monto" placeholder="monto" min="0" step=".01" onchange="cambiarMonto()" required>
				Valor total (AR$):
                <input type="number" class="form-control" id="total" name="total" value="0" step=".01" required readonly>
				<br />
                <button class="btn btn-lg btn-primary btn-block" id="invertir" type="submit">Invertir</button>
            </div>  

			<div class="form-group col-md-4" style="display: inline-block"> 
				Valor para la compra: AR$ ${producto.cotizacionCompra} <br />
				Valor para la venta:<strong> AR$ ${producto.cotizacionVenta}</strong>
            </div> 
        </g:form>

	</div>

	<script>
		function cambiarMonto(){
			let monto = document.getElementById("monto").value;
			let montoProd = document.getElementById("montoProd").value;
			if(monto * montoProd < 0)
				document.getElementById("invertir").disabled=true
			else
				document.getElementById("invertir").disabled=false
			document.getElementById("total").value = monto * montoProd;
		}
	</script>
</body>