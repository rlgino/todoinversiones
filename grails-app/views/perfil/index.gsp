<!doctype html>
<html xmlns:g="http://www.w3.org/1999/XSL/Transform">
    <head>
        <meta name="layout" content="main" />
        <title>${session.usuario.nombre} ${session.usuario.apellido}</title>
        <link href="https://getbootstrap.com/docs/4.3/examples/floating-labels/floating-labels.css" rel="stylesheet">
    </head>
<body>
    <form class="form-signin" >
        <div class="text-center mb-4">
          <img class="mb-4" src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        </div>
        <h1>${session.usuario.nombre} ${session.usuario.apellido}<span class="badge badge-secondary">$${ saldo }</span></h1>

        <div class="form-label-group">
            <label>Documento ${session.usuario.dni}</label>
        </div><br />
        <div class="form-label-group">
            <label>Banco <b>${ banco }</b></label>
        </div>
        <br />
        <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                Cargar saldo!
            </a>
        </p>
        <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    Deberia aparecer opciones para traer desde aplicación externa
                    <div class="input-group mb-3">
            <g:form  action="cargar" controller="perfil">
                        <input type="number" min="0" class="form-control" name="saldo" placeholder="Nuevo saldo" aria-label="Nuevo saldo" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Cargar</button>
                        </div>
            </g:form>
                    </div>
                </div>
        </div>
    </form>
</body>
</html>
