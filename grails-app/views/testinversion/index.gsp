<html>
<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">
<head>
    <meta name="layout" content="main" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Cuestionario:Perfil inversor.</title>
</head>
<body class="d-flex flex-column h-100">
<h1>Cuestionario</h1>
<div id="page-wrap">

		<h1>Defina su Perfil inversor </h1>

        <g:form class="form-quiz" action="calcularPerfil" id="quiz" controller="testinversion">
        
            <g:if test="${flash.message}">
                <div class="alert alert-danger" role="alert">
                    ${flash.message}
                </div>
            </g:if>  

            <input type="hidden" name="cantPreguntas" value="${ preguntas.size() }" />
		
            <ol>
            
            <g:each in="${preguntas}" var="pregs">

                <li>
                
                    <h3> "${pregs.pregunta}" </h3>
                    <div >
                    
                        <g:radio name="op${pregs.id}" value= "${pregs.puntajeOpcionA}"/> "${pregs.opcionA}" <br>
                        
                    </div>
                    
                    <div>
                        <g:radio name="op${pregs.id}" value="${pregs.puntajeOpcionB}"/> "${pregs.opcionB}"<br>
                    </div>
                    
                    <div>
                        <g:radio name="op${pregs.id}" value="${pregs.puntajeOpcionC}"/> "${pregs.opcionC}"<br>
                    </div>
                    
                </li>
               
            </g:each >
            
            </ol>
            
            <g:submitButton name="submitButton" href="/mostrarResultado" value="Enviar respuestas." />
		
		</g:form>

    </div>    

</body>
</html>