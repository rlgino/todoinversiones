<html>

	<html lang="en" class="h-100" xmlns:g="http://www.w3.org/1999/xhtml">

	<head>

		<meta name="layout" content="main" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Perfil inversor </title>

	</head>

	<body class="d-flex flex-column h-100">

		<h2>Resultado del cuestionario</h2>                                              

		<div class="alert alert-success">
  			<strong>Perfil encontrado ! </strong> ${usuario.nombre} tu perfil inversor es ${cliente.perfilInversor}.
		</div>

		<br/>

		<a class="nav-link" href="/bienvenida/index">Volver al inicio.</a>

	</body>	

</html>