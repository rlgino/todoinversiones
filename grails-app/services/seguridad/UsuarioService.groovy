package seguridad

import grails.gorm.transactions.Transactional
import todoinversiones.Usuario

@Transactional
class UsuarioService {

    static Usuario usuario

    def static listar(){
        return Usuario.findAll() as Usuario[]
    }
}
