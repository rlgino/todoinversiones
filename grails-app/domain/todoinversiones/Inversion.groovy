package todoinversiones

class Inversion {

    Long id
    Producto producto
    Double monto
    Date fechaInversion
    Date fechaFin
    Cliente cliente

    static constraints = {
        fechaFin nullable: true
    }
}
