package todoinversiones

class Producto {

    Long id
    String nombre
    BigDecimal cotizacionVenta
    BigDecimal cotizacionCompra
    String descripcion
    Categoria categoria

    static constraints = {
    }
}
