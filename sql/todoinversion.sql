create database todoinversion;

create table usuario(
    id serial,
    nombre text,
    apellido text,
    fechaNac date,
    mail text not null UNIQUE,
    cont text,
    esCeo boolean);

alter table usuario add CONSTRAINT USUARIO_PK PRIMARY key (id);

create table cliente (
    id serial,
    id_usuario int,
    banco text,
    hizoEncuesta boolean
);

alter table cliente ADD CONSTRAINT cliente_usuario_fk FOREIGN key (id_usuario) references usuario(id);

create table categoria (
    id serial,
    nombre text
);
alter table categoria ADD CONSTRAINT  categoria_pk PRIMARY key (id);

create table producto (
    id serial,
    nombre text,
    cotizacionVenta decimal(8,2),
    cotizacionCompra decimal(8,2),
    descripcion text,
    categoria int
);

alter table producto  ADD CONSTRAINT  producto_pk PRIMARY key (id);

create table encuesta( 
    mail text,
    pregunta text,
    respuesta text
);

alter table usuario add column es_admin boolean default false;
alter table producto add CONSTRAINT prodcategoria_fk foreign key (categoria) references categoria(id);

create table compraproducto(id_usuario int, id_producto int,cant_compra decimal(8,2),precio decimal(8,2));

alter table compraproducto add constraint compraproducto_pk primary key(id_usuario,id_producto);

alter table compraproducto add constraint compraprod_id_usuario_fk foreign key (id_usuario) references usuario(id);

alter table compraproducto add constraint compraprod_producto_fk foreign key (id_producto) references producto(id);
